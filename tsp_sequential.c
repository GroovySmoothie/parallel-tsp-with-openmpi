/** 
 * tsp_sequential 1.0
 * ----------------------------
 *		tsp_sequential implements Branch and Bound through Deep First Search on a graph G. 
 *		G has weighted paths D between city i and city j. Thus D[i][j] is the weight of the 
 *		path between city i and city j. The total number of cities is N. The program works 
 * 		on a weighted path matrix which represents the graph to find the minimum distance to 
 * 		traverse all cities onces, without backtracking and starting from city 1. 
 *
 *		usage: 	./tsp_sequential file_name
 * 				./tsp_sequential log file_name
 *				./tsp_sequential file_name log
 *
 *		author:		Jozsef Kepes
 *		version:	1.0
 *		since: 		2018-05-02
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

/** 
 *		bound: 			Upper bound to compare weight with (denotes best solution found so far)
 *		basecase: 		Iterator to detect bottom of branch
 *		final_path:		Array to store current best path 
 * 		log_flag:		Flag to toggle log output
 */
double bound = INFINITY;;
int basecase = 0;
int* final_path;
int log_flag = 0;
long prune = 0;

/*
 * Function: branchBound
 * ----------------------------
 *   Traverses the matrix(tree) using Depth First Search and applies the
 *	 branch and bound algorithm. Refer to design_notes.txt for streamlined 
 *   outline of this function.
 *
 *   input_matrix: matrix of graph weighted values
 *   weight: weight between two vertices
 *	 visited: tracks visited state for current node's neighbours
 *   node: current vertex
 *   path: current traversal path
 *
 *   returns: none
 */
void branchBound(double** input_matrix, double weight, double dimensions, int* visited, int node, int* path)
{
	int i,j;
	int index = dimensions-basecase;

	//Add node to current traversal path
	path[index] = node+1;
	basecase--;

	//If the bottom of a branch(leaf) has been reached
	if(basecase == 0)
	{
		if(weight < bound)
		{
			 //Store current path
			for(i=0; i<dimensions; i++)
			{
				final_path[i] = path[i];
			}
			//Set new minimum cumulative path weight
			bound = weight;
		}
		return;
	}

	//Mark current node as visited
	visited[node] = 1;

	//Start DFS
	for(i=0; i<dimensions; i++)
	{
		if(!visited[i])
		{
			if(weight < bound)
			{
				//Sum path weigh and call BnB on neighbours not visited
				weight += input_matrix[i][node];		
				branchBound(input_matrix,weight,dimensions,visited,i, path);
				weight -= input_matrix[i][node];
				basecase++;
			}
			else
			{
				if(log_flag == 1)
				{
					//add pruned path to log
					prune++;
					// FILE *f = fopen("log", "a");
					// if (f == NULL)
					// {
					// 	printf("Error opening file!\n");
					// 	exit(1);
					// }

					// for(j=0; j<=index; j++)
					// {
					// 	if(j != index)
					// 	{
					// 		fprintf(f,"%d,",path[j]);
					// 	}
					// 	else
					// 	{
					// 		fprintf(f,"%d\n",path[j]);
					// 	}
					// }
					// fclose(f);
				}
			}
		}

	}
	//reset visited node after it has been used
	visited[node] = 0;
	return;
}


/*
 * Function: errExit1
 * ----------------------------
 *   Traverses the matrix(tree) using Depth First Search  and applies the
 *	 branch and bound algorithm.
 *
 *   s1: Custom string to describe error
 *   s2: String to preceed system error message
 *
 *   returns: none
 */
void errExit1(char* s1)
{
	fprintf(stderr,"Error: %s \n", s1);
	exit(1);
}

/*
 * Function: errExit2
 * ----------------------------
 *   Traverses the matrix(tree) using Depth First Search  and applies the
 *	 branch and bound algorithm.
 *
 *   s1: Custom string to describe error
 *   s2: String to preceed system error message
 *
 *   returns: none
 */
void errExit2(char* s1, char* s2)
{
	fprintf(stderr,"Error: %s ", s1);
	perror(s2);
	printf("\n");
	exit(1);
}

/*
 * Function: main
 * ----------------------------
 *   This is the main method which reads input from a file, allocates the appropriate memory
 *   for variables, calls the branch and bound method on the first node and then cleans up 
 *   memory allocation.
 *
 *   argc: Number of runtime variables
 *   argv: Contains the runtime variables for input_file and log
 *
 *   returns: none
 */
int main(int argc, char* argv[])
{

	//Start command line argument operations
	FILE* file;
	if(argc == 2)
	{
		if((file = fopen(argv[1], "r")) == NULL)
		{
			errExit2("Read error from file: ",argv[1]);
		}
	}
	else if(argc == 3)
	{
		int i;
		for(i=0; i<argc; i++)
		{
			if(strncmp(argv[i],"log\0", 4) == 0)
			{
				log_flag =1;
			}
			else
			{
				if((file = fopen(argv[i], "r")) == NULL)
				{
					errExit2("Read error from file: ",argv[i]);
				}
			}
		}
	}
	else
	{
		errExit1("Incorrect command line arguments. Please use the format: ");
	}
	//End input file reading operations

	char line[1024];
	char *token;
	char *temp;
	int tokenCount = 0;
	int* visited_nodes;
	int* path;
	double n_value;
	double** path_weights;
	int i,j;

	//Set up weighted paths matrix dimension value and max traversal depth
	fscanf(file,"%lf", &n_value);
	basecase = n_value;

	//Set up 2D array for weighted paths matrix from input
	path_weights = malloc(n_value * sizeof(double*));
	for (i=0; i<n_value; i++) 
	{
	  path_weights[i] = malloc(n_value * sizeof(double));
	}
	for(i=0; i<n_value ;i++)
	{
		path_weights[i][i] = INFINITY;
		for (j = 0; j < i; j++)
		{
			double weight;
			fscanf(file,"%lf", &weight);
			path_weights[i][j] = weight;
			path_weights[j][i] = weight;
		}
	}

	//Allocate memory for dynamic arrays
	visited_nodes = malloc(n_value * sizeof(double));
	for(i=0; i<n_value; i++)
	{
		visited_nodes[i] = 0;
	}

	path = malloc(n_value * sizeof(double));
	for(i=0; i<n_value; i++)
	{
		path[i] = 0;
	}

	final_path = malloc(n_value * sizeof(double));
	for(i=0; i<n_value; i++)
	{
		final_path[i] = 0;
	}
	
	//Call branch and bound on first node and time execution
	clock_t start = clock(), duration;
	branchBound(path_weights, 0, n_value, visited_nodes, 0, path);
	duration = clock() - start;
	int time = duration * 1000/CLOCKS_PER_SEC;

	//Print execution time in milliseconds
	printf ("BnB Time = %d\n", time%1000);

	for(i=0; i<n_value; i++)
	{
		if(i != n_value-1)
		{
			printf("%d,",final_path[i]);
		}
		else
		{
			printf("%d\n",final_path[i]);
		}
	}
	printf("Distance: %1.0f\n",bound);

	printf("Prune: %ld\n",prune);

	//Delete allocated memory for dynamic arrays
	free(final_path);
	free(path);
	free(visited_nodes);
	for (i=0; i<n_value; i++) {
	  free(path_weights[i]);
	}
	free(path_weights);

	return 0;
}

