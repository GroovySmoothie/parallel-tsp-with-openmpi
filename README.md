# Parallel TSP

## How to use:

Some randomly generated test files have been included in ./test

test-x-y.txt  
where x is the number of cities  
and y is the id for x

### tsp_sequential.c

Compile as normal with gcc:  
gcc tsp_sequential -o tsp_sequential

Run as usual:  
./tsp_sequential

Arguments [test-file] ?["log"]

[test-file] The file containing the test with the format:  
N  
d[1][2]  
d[1][3] d[2][3]  
d[1][4] d[2][4] d[3][4]  
...  
d[1][N] d[2][N] d[3][N] ... d[N-1][N]

?["log"] Optional. The word "log" will cause the program to print all cut branches to a file called log

### tsp_parallel.c

Compile with mpicc:  
mpicc tsp_parallel -o parallel

Run with mpiexec:  
mpiexec -np #num_of_processes parallel

Arguments [test-file] ?["log"]

[test-file] The file containing the test with the format:  
N  
d[1][2]  
d[1][3] d[2][3]  
d[1][4] d[2][4] d[3][4]  
...  
d[1][N] d[2][N] d[3][N] ... d[N-1][N]

?["log"] Optional. The word "log" will cause the program to print all cut branches to a file called log