/** 
 * tsp_sequential 1.0
 * ----------------------------
 *		tsp_sequential implements Branch and Bound through Deep First Search on a graph G. 
 *		G has weighted paths D between city i and city j. Thus D[i][j] is the weight of the 
 *		path between city i and city j. The total number of cities is N. The program works 
 * 		on a weighted path matrix which represents the graph to find the minimum distance to 
 * 		traverse all cities onces, without backtracking and starting from city 1. 
 *
 *		usage: 	./tsp_sequential file_name
 * 				./tsp_sequential log file_name
 *				./tsp_sequential file_name log
 *
 *		author:		Jozsef Kepes
 *		version:	1.0
 *		since: 		2018-05-02
 */
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <mpi.h>


#define MSG_PATH 1
#define MSG_DISTANCE 2

/** 
 *		bound: 			Upper bound to compare weight with (denotes best solution found so far)
 *		basecase: 		Iterator to detect bottom of branch
 *		final_path:		Array to store current best path 
 * 		log_flag:		Flag to toggle log output
 */
double bound = INFINITY;;
int basecase = 0;
int* final_path;
int log_flag = 0;
int numSlaves = 0;
int numProcessors = 0;
int slave = 0;
int num_subtasks = 0;
int prune = 0;
/*
 * Function: branchBound
 * ----------------------------
 *   Traverses the matrix(tree) using Depth First Search and applies the
 *	 branch and bound algorithm. Refer to design_notes.txt for streamlined 
 *   outline of this function.
 *
 *   input_matrix: matrix of graph weighted values
 *   weight: weight between two vertices
 *	 visited: tracks visited state for current node's neighbours
 *   node: current vertex
 *   path: current traversal path
 *
 *   returns: none
 */
void branchBound(double** input_matrix, double weight, double dimensions, int* visited, int node, int* path, int break_value)
{
	int i,j;
	int index = dimensions-basecase;

	//Add node to current traversal path
	path[index] = node+1;
	basecase--;

	//If the bottom of a branch(leaf) has been reached
	if(basecase == 0)
	{		
		if(weight < bound)
		{
			 //Store current path
			for(i=0; i<dimensions; i++)
			{
				
				final_path[i] = path[i];
			}

			//Set new minimum cumulative path weight
			bound = weight;
		}
	}
	else
	{
		//Mark current node as visited
		visited[node] = 1;

		//Start DFS
		for(i=0; i<dimensions; i++)
		{
			if(!visited[i])
			{ 
				//printf("weight: %f , bound: %f \n" , weight, bound); 
				if(weight < bound)
				{	 
					if(index == break_value)
					{
						int rank;
						MPI_Comm_rank(MPI_COMM_WORLD, &rank);
						weight += input_matrix[i][node];
						num_subtasks++;
						path[index+1] = i+1;

						int* value;
						MPI_Alloc_mem(sizeof(int), MPI_INFO_NULL, &value);
						value[0] = weight;

						//instead of sending to 1 send to available processors
						//need a value that flips everytime it enters
						
						int send;	
						send = slave%numSlaves + 1;
						MPI_Send(path, dimensions, MPI_INT, send, MSG_PATH, MPI_COMM_WORLD);
						MPI_Send(value, 1, MPI_INT, send, MSG_DISTANCE, MPI_COMM_WORLD);
						MPI_Free_mem(value);	
						weight -= input_matrix[i][node];
						slave++;
					}
					else
					{
						//Sum path weight and call BnB on neighbours not visited
						weight += input_matrix[i][node];
						int rank;
						MPI_Comm_rank(MPI_COMM_WORLD, &rank);			
						branchBound(input_matrix, weight, dimensions, visited, i, path, break_value);
						weight -= input_matrix[i][node];
						basecase++;
					}
				}
				else
				{
					if(log_flag == 1)
					{
						//add pruned path to log
						prune++;
						// FILE *f = fopen("log", "a");
						// if (f == NULL)
						// {
						// 	printf("Error opening file!\n");
						// 	exit(1);
						// }

						// for(j=0; j<=index; j++)
						// {
						// 	if(j != index)
						// 	{
						// 		fprintf(f,"%d,",path[j]);
						// 	}
						// 	else
						// 	{
						// 		fprintf(f,"%d\n",path[j]);
						// 	}
						// }
						// fclose(f);
					}
				}
			}
		}
		//reset visited node after it has been used
		visited[node] = 0;
	}
	return;
}

/*
 * Function: errExit1
 * ----------------------------
 *   Traverses the matrix(tree) using Depth First Search  and applies the
 *	 branch and bound algorithm.
 *
 *   s1: Custom string to describe error
 *
 *   returns: none
 */
void errExit1(char* s1)
{
	fprintf(stderr,"Error: %s \n", s1);
	exit(1);
}

/*
 * Function: errExit2
 * ----------------------------
 *   Traverses the matrix(tree) using Depth First Search  and applies the
 *	 branch and bound algorithm.
 *
 *   s1: Custom string to describe error
 *   s2: String to preceed system error message
 *
 *   returns: none
 */
void errExit2(char* s1, char* s2)
{
	fprintf(stderr,"Error: %s ", s1);
	perror(s2);
	printf("\n");
	exit(1);
}

/*
 * Function: main
 * ----------------------------
 *   This is the main method which reads input from a file, allocates the appropriate memory
 *   for variables, calls the branch and bound method on the first node and then cleans up 
 *   memory allocation.
 *
 *   argc: Number of runtime variables
 *   argv: Contains the runtime variables for input_file and log
 *
 *   returns: int
 */
int main(int argc, char* argv[])
{
	//Initialise the MPI environment
	MPI_Init(NULL, NULL);

	//Start command line argument operations
	FILE* file;
	if(argc == 2)
	{
		if((file = fopen(argv[1], "r")) == NULL)
		{
			errExit2("Read error from file: ",argv[1]);
		}
	}
	else if(argc == 3)
	{
		int i;
		for(i=0; i<argc; i++)
		{
			if(strncmp(argv[i],"log\0", 4) == 0)
			{
				log_flag =1;
			}
			else
			{
				if((file = fopen(argv[i], "r")) == NULL)
				{
					errExit2("Read error from file: ",argv[i]);
				}
			}
		}
	}
	else
	{
		errExit1("Incorrect command line arguments. Please use the format: ");
	}
	//End input file reading operations

	char line[1024];
	char *token;
	char *temp;
	int tokenCount = 0;
	int* visited_nodes;
	int* path;
	double n_value;
	double** path_weights;
	int i,j;
	int current_distance;

	//Set up weighted paths matrix dimension value and max traversal depth
	fscanf(file,"%lf", &n_value);
	basecase = n_value;

	//Set up 2D array for weighted paths matrix from input
	MPI_Alloc_mem(n_value * sizeof(double*), MPI_INFO_NULL, &path_weights);
	for (i=0; i<n_value; i++) 
	{
	MPI_Alloc_mem(n_value * sizeof(double), MPI_INFO_NULL, &path_weights[i]);
	}
	for(i=0; i<n_value ;i++)
	{
		path_weights[i][i] = INFINITY;
		for (j = 0; j < i; j++)
		{
			double weight;
			fscanf(file,"%lf", &weight);
			path_weights[i][j] = weight;
			path_weights[j][i] = weight;
		}
	}

	//Allocate memory for dynamic arrays
	MPI_Alloc_mem(n_value * sizeof(double), MPI_INFO_NULL, &visited_nodes);
	for(i=0; i<n_value; i++)
	{
		visited_nodes[i] = 0;
	}
	MPI_Alloc_mem(n_value * sizeof(double), MPI_INFO_NULL, &path);
	for(i=0; i<n_value; i++)
	{
		path[i] = 0;
	}
	MPI_Alloc_mem(n_value * sizeof(double), MPI_INFO_NULL, &final_path);
	for(i=0; i<n_value; i++)
	{
		final_path[i] = 0;
	}

	//BEGIN MPI
	int rank;
	int static_depth = 2;

	//Retreive rank and store
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
	numSlaves = numProcessors - 1;
	
	//Rank 0 is the master, otherwise its a slave
	if(rank == 0)
	{
		MPI_Status status;

		//Call branch and bound on first node and time execution
		//clock_t start = clock(), duration;

		//Do inital BNB up to static depth
		branchBound(path_weights, 0, n_value, visited_nodes, 0, path, static_depth);

		int rank;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);

		//get number of processors
		MPI_Comm_size(MPI_COMM_WORLD, &numProcessors);
		numSlaves = numProcessors - 1;

		while(1)
		{
			//Receive slave outcome
			MPI_Recv(path, n_value, MPI_INT, MPI_ANY_SOURCE, MSG_PATH, MPI_COMM_WORLD, &status);
			if(status.MPI_TAG == MSG_PATH)
			{ 	
				if(path[0] == -1)
				{
					break;
				}
				else
				{
					//printf("MASTER GOT A PATH from %d \n", status.MPI_SOURCE);
					for(i=0; i<n_value; i++)
					{
						if(i != n_value-1)
						{
							//printf("%d,",path[i]);
						}
						else
						{
							//printf("%d\n",path[i]);
						}
					}
				}	
			}

			MPI_Recv(&current_distance, 1, MPI_INT, MPI_ANY_SOURCE, MSG_DISTANCE, MPI_COMM_WORLD, &status);
			if(status.MPI_TAG == MSG_DISTANCE)
			{
				if(current_distance == -1)
				{
					break;
				}
				else
				{
					//printf("MASTER GOT A DISTANCE from %d \n", status.MPI_SOURCE);
					//printf("bound: %d\n",current_distance);
				}
					
			}

			//maybe do this somewhere else?? 
			if(current_distance <= bound)
			{
				//printf("NEW MINIMUM");
				bound = current_distance;
				for(i=0; i<n_value; i++)
				{
					final_path[i] = path[i];
				}	
			}

			num_subtasks--;
			//printf ("Subtasks = %d\n", num_subtasks);
			if(num_subtasks == 0)
			{
				//duration = clock() - start;
				//int time = duration * 1000/CLOCKS_PER_SEC;
				//Print execution time in milliseconds
				//printf ("BnB Time = %d\n", time%1000);

				//printf("SHUTDOWN INITIATED\n");
				for(i=1; i<=numSlaves; i++)
				{
					int* value;
					MPI_Alloc_mem(sizeof(int), MPI_INFO_NULL, &value);
					value[0] = -1;
					path[0] = -1;
					MPI_Send(path, n_value, MPI_INT, i, MSG_PATH, MPI_COMM_WORLD);
					MPI_Send(value, 1, MPI_INT, i, MSG_DISTANCE, MPI_COMM_WORLD);
					MPI_Free_mem(value);
				}
			}
		}

		printf("Master final path: ");
		for(i=0; i<n_value; i++)
		{
			if(i != n_value-1)
			{
				printf("%d,",final_path[i]);
			}
			else
			{
				printf("%d\n",final_path[i]);
			}
		}

		printf("Distance: %1.0f\n",bound);
	}
	else
	{
		while(1)
		{
			MPI_Status status;
			for(i=0; i<n_value; i++)
			{
				path[i] = 0;
			}

			//Receive path
			MPI_Recv(path,n_value, MPI_INT, 0, MSG_PATH, MPI_COMM_WORLD, &status);
			if(status.MPI_TAG == MSG_PATH)
			{
				if( path[0] == -1)
				{
					int* value;
					MPI_Alloc_mem(sizeof(int), MPI_INFO_NULL, &value);
					value[0] = -1;
					path[0] = -1;
					MPI_Send(path, n_value, MPI_INT, 0, MSG_PATH, MPI_COMM_WORLD);
					MPI_Send(value, 1, MPI_INT, 0, MSG_DISTANCE, MPI_COMM_WORLD);
					MPI_Free_mem(value);
					break;
				}
				else
				{
					//Flush visited nodes from previous run
					for(i=0; i<n_value; i++)
					{
						visited_nodes[i] = 0;
					}

					current_distance = 0;

					for(i=0; i<n_value; i++)
					{
						//Set visited path array
						if(path[i] != 0)
						{
							visited_nodes[path[i]-1] = 1;
						}
					}
				}		
			}

			//Receive distance
			MPI_Recv(&current_distance,1, MPI_INT, 0, MSG_DISTANCE, MPI_COMM_WORLD, &status);
			if(status.MPI_TAG == MSG_DISTANCE)
			{
				if(current_distance == -1)
				{
					int* value;
					MPI_Alloc_mem(sizeof(int), MPI_INFO_NULL, &value);
					value[0] = -1;
					path[0] = -1;
					MPI_Send(path, n_value, MPI_INT, 0, MSG_PATH, MPI_COMM_WORLD);
					MPI_Send(value, 1, MPI_INT, 0, MSG_DISTANCE, MPI_COMM_WORLD);
					MPI_Free_mem(value);
					break;
				}
			}
			basecase = n_value-static_depth-1;	
			int temp = path[static_depth];
			branchBound(path_weights, current_distance, n_value, visited_nodes, path[static_depth+1]-1, path, n_value+1);
			int* test;
			MPI_Alloc_mem(sizeof(int), MPI_INFO_NULL, &test);
			test[0] = bound;
			MPI_Send(final_path, n_value, MPI_INT, 0, MSG_PATH, MPI_COMM_WORLD);
			MPI_Send(test, 1, MPI_INT, 0, MSG_DISTANCE, MPI_COMM_WORLD);
			MPI_Free_mem(test);
		}
	}

	//END MPI
	
	//Delete allocated memory for dynamic arrays
	MPI_Free_mem(final_path);
	MPI_Free_mem(path);
	MPI_Free_mem(visited_nodes);
	for (i=0; i<n_value; i++) {
	  MPI_Free_mem(path_weights[i]);
	}
	MPI_Free_mem(path_weights);

	MPI_Finalize();
	
	return 0;
}

